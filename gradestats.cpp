#include <iostream>
#include <vector>

using namespace std;

int main(){

  vector <int> allGrades;
  int input, total, average, counter = 0;

  while (input != 0){

    cout << "Enter a grade (or -1 to exit): ";

    cin >> input;
    if (input < 0 && input != -1){
      cout << "Error; value cannot be negative." << endl;
      continue;
    }
    else if (input > 0){
      allGrades.push_back(input);
    }
    else if (input == -1){
      for (counter;counter < allGrades.size(); counter++){
	total += allGrades.at(counter);
      }
      average = total/counter;
      break;
    }

  }

  cout << "Your average grade is " << average;
  
}
