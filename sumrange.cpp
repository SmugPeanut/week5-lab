#include <iostream>

using namespace std;

int main(){

  int sum;

  cout << "Please enter 2 numbers: ";

  int firstVal, secondVal;

  cin >> firstVal;
  cin >> secondVal;

  int rangeStart, rangeEnd;

  while (firstVal == secondVal){
    cout << "Please enter 2 different numbers:";
    cin >> firstVal;
    cin >> secondVal;
  }
  if (firstVal > secondVal){
    rangeStart = secondVal;
    rangeEnd = firstVal;
  } else if (secondVal > firstVal){
    rangeStart = firstVal;
    rangeEnd = secondVal;
  }

  int finalSum = rangeStart;
  
  for (int i = rangeStart; i < rangeEnd; i++){
    finalSum += (i+1);
  }

  cout << "The sum of values from " << rangeStart << " and " << rangeEnd << " is " << finalSum;
}
